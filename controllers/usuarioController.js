const Usuario = require('../models/Usuario');
const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');

exports.crearUsuario = async (req, res) => {
    
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() });
    }

    
    // Extraemos email y password
    let { email, password } = req.body;
    
    try {
        let usuario = await Usuario.findOne({ email });

        console.log(usuario);
        if(usuario){
            return res.status(400).json({ msg: 'El email ingresado ya existe.' });
        }

        // Crear un usuario
        usuario = new Usuario(req.body);
        usuario.password = bcryptjs.hashSync(password, 10);

        // Guarda el usuario en la BD
        await usuario.save();

        // Crear el payload
        const payload = {
            usuario: {
                id: usuario.id
            }
        }

        // Firmar y retornar el JWT
        jwt.sign(payload, process.env.SECRET, { 
            expiresIn: "180d"
        }, (error, token) => {
            // En caso de error
            if(error) throw error;

            // Mensaje de confirmación
            res.json({ msg: 'Usuario creado correctamente.', token, usuario });
        })

    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};