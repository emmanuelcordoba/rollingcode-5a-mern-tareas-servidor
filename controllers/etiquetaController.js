const mongoose = require('mongoose');
const Etiqueta = require('../models/Etiqueta');
const { validationResult } = require('express-validator');

exports.crearEtiqueta = async (req, res) => {
    
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() });
    }
    
    try {
        // Crear un etiqueta
        etiqueta = new Etiqueta(req.body);

        //Guardamos el usuario
        etiqueta.usuario = req.usuario.id;

        // Guardar la etiqueta en la BD
        await etiqueta.save();
        res.json({ msg: 'Etiqueta creada correctamente.', etiqueta });

    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.listar = async (req, res) => {
    
    try {
        const etiquetas = await Etiqueta.find({ usuario: req.usuario.id }).sort('nombre');
        res.json({ etiquetas });
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.eliminar = async (req, res) => {
    
    try {        
        // Validar el ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La etiqueta no existe.' });
        }
        
        // Varificar que la Etiqueta exista
        let etiqueta = await Etiqueta.findById(req.params.id);

        if(!etiqueta){
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }

        // Verificar que la etiqueta le pertenezca al usuario autenticado
        if(etiqueta.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Eliminar Etiqueta
        await etiqueta.remove();
        res.json({ msg: 'Etiqueta eliminada correctamente.' });
        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};