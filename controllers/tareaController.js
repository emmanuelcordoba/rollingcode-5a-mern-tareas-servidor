const mongoose = require('mongoose');
const Tarea = require('../models/Tarea');
const Etiqueta = require('../models/Etiqueta');
const { validationResult } = require('express-validator');

exports.crearTarea = async (req, res) => {
    
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() });
    }
    
    try {
        // Controlar la etiqueta
        const etiqueta = await Etiqueta.findById(req.body.etiqueta);
        if(!etiqueta){
            return res.status(400).json({ msg: 'La Etiqueta no existe.' })
        }

        // Controlar si la etiquea pertece al usuario autenticado
        if(etiqueta.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Crear una tarea
        tarea = new Tarea(req.body);
        
        // Guarda la tarea en la BD
        await tarea.save();
        res.json({ msg: 'Tarea creada correctamente.', tarea });
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.listarPorEtiqueta = async (req, res) => {
    
    const { page } = req.query
    
    try {
        //Controlar la etiqueta
        console.log(req.params.id);
        const etiqueta = await Etiqueta.findById(req.params.id);
        if(!etiqueta){
            return res.status(404).json({ msg: 'La etiqueta no existe.'});
        }

        // Controlar si la etiquea pertece al usuario autenticado
        if(etiqueta.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        const options = {
            page: page ? page : 1,
        };

        const tareas = await Tarea.paginate({ etiqueta: etiqueta.id },options);
        res.json(tareas);
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.actualizar = async (req, res) => {
    
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() });
    }

    // Extraer la etiqueta
    const { etiqueta } = req.body;
    
    try {        
        // Validar el ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }
        
        // Varificar que la Tarea exista
        let tarea = await Tarea.findById(req.params.id).populate('etiqueta');

        if(!tarea){
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }

        // Verificar que la tarea le pertenezca al usuario autenticado
        // Esto es verificar que la tarea sea de una etiqueta del usuario
        if(tarea.etiqueta.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Controlar que la Etiqueta exista
        const etiquetaExiste = await Etiqueta.findById(etiqueta);
        if(!etiquetaExiste){
            return res.status(400).json({ msg: 'La etiqueta no existe.' })
        }

        // Controlar que la Etiqueta le pertenezca al Usuario autenticado
        if(etiquetaExiste.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Modificar la Tarea
        tarea = await Tarea.findByIdAndUpdate(req.params.id, req.body, { new: true})

        res.json({ msg: 'Tarea modificada correctamente.', tarea });
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.eliminar = async (req, res) => {
    
    try {        
        // Validar el ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }
        
        // Varificar que la Tarea exista
        let tarea = await Tarea.findById(req.params.id).populate('etiqueta');

        if(!tarea){
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }

        // Verificar que la tarea le pertenezca al usuario autenticado
        // Esto es verificar que la tarea sea de una etiqueta del usuario
        if(tarea.etiqueta.usuario.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Eliminar Tarea
        await tarea.remove();
        res.json({ msg: 'Tarea eliminada correctamente.' });
        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};