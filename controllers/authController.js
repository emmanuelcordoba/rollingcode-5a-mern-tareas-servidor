const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');

exports.login = async (req, res) => {

    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() });
    }

    // Extraemos email y password
    let { email, password } = req.body;

    try {
        // Revisar que sea un usuario registrado
        let usuario = await Usuario.findOne({ email });

        if(!usuario){
            console.log('El usuario no existe.');
            return res.status(400).json({ msg: 'El email o contraseña son incorrectos.' });
        }

        // Verificamos el password
        const passCorrecto = await bcryptjs.compare(password, usuario.password);
        if(!passCorrecto){
            console.log('El password no es válido.');
            return res.status(400).json({ msg: 'El email o contraseña son incorrectos.' });
        }

        // Crear el payload
        const payload = {
            usuario: {
                id: usuario.id
            }
        }

        // Firmar y retornar el JWT
        jwt.sign(payload, process.env.SECRET, { 
            expiresIn: "180d"
        }, (error, token) => {
            // En caso de error
            if(error) throw error;

            // Mensaje de confirmación
            res.json({ token, usuario });
        });
        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' });
    }    
};