const express = require('express');
const router = express.Router();
const usuarioController = require('./controllers/usuarioController');
const etiquetaController = require('./controllers/etiquetaController');
const tareaController = require('./controllers/tareaController');
const authController = require('./controllers/authController');
const auth = require('./middlewares/auth');
const { check } = require('express-validator');

// Crear un usuario
// /api/usuarios/
router.post('/usuarios/', [
        check('nombre','El nombre es obligatorio.').not().isEmpty(),
        check('email','El email es obligatorio.').not().isEmpty(),
        check('email','Ingrese un email válido.').isEmail(),
        check('password','El password es obligatorio.').not().isEmpty(),
        check('password','El password debe ser mínimo de 6 caracteres.').isLength({ min: 6})
    ],
    usuarioController.crearUsuario
);

// Autenticación de usuario
// /api/auth/
router.post('/auth/', [
        check('email','El email es obligatorio.').not().isEmpty(),
        check('email','Ingrese un email válido.').isEmail(),
        check('password','El password es obligatorio.').not().isEmpty()
    ],
    authController.login
);

// Crear una etiqueta
// /api/etiquetas/
router.post('/etiquetas/', 
    auth,
    [
        check('nombre','El nombre es obligatorio.').not().isEmpty()
    ],
    etiquetaController.crearEtiqueta
);

// Obtener etiquetas
// /api/etiquetas/
router.get('/etiquetas/', 
    auth,
    etiquetaController.listar
);

// Crear tarea
// /api/tareas/
router.post('/tareas/', 
    auth,
    [
        check('nombre','El nombre es obligatorio.').not().isEmpty(),
        check('etiqueta','El etiqueta es obligatoria.').not().isEmpty(),
        check('etiqueta','El etiqueta no es válida.').isMongoId(),
    ],
    tareaController.crearTarea
);

// Modificar tarea
// /api/tareas/:id
router.put('/tareas/:id', 
    auth,
    [
        check('nombre','El nombre es obligatorio.').not().isEmpty(),
        check('finalizada','El campo finalziada es obligatorio.').not().isEmpty(),
        check('etiqueta','El etiqueta es obligatoria.').not().isEmpty(),
        check('etiqueta','El etiqueta no es válida.').isMongoId(),
    ],
    tareaController.actualizar
);

// Eliminar tarea
// /api/tareas/:id
router.delete('/tareas/:id', 
    auth,
    tareaController.eliminar
);

// Listar tareas de una etiqueta
// /api/etiquetas/:id/tareas
router.get('/etiquetas/:id/tareas', 
    auth,
    tareaController.listarPorEtiqueta
);

// Eliminar etiqueta
// /api/etiquetas/:id
router.delete('/etiquetas/:id', 
    auth,
    etiquetaController.eliminar
);

module.exports = router;