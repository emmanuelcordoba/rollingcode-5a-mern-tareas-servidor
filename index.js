const express = require('express');
const dotenv = require('dotenv');
dotenv.config();
const morgan = require('morgan');
const mongoose = require('./database');
const enviarEmail = require('./enviarEmail');
const cors = require('cors');

// Crear la aplicacion
const app = express();

// Puerto de la app
const PORT = process.env.PORT || 4000;

// Middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json({ limit: '10mb' })); // for parsing application/json
app.use(express.urlencoded({ limit: '10mb', extended: true})); // for parsing application/x-www-form-urlencoded

// Rutas
app.use('/api', require('./routes'));

// Ruta de prueba
app.post('/api/enviaremail', async (req,res) => {

    const { email, subject, msg }  = req.body
    try {       

        // Enviar el email
        let info = await enviarEmail(email, subject, msg);
        console.log("info",info);

        res.json({ msg: 'Email enviado.'});        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
});

// Inicial la app
app.listen(PORT,'0.0.0.0',() => {
    console.log(`El servidor está funcionando en el perto ${PORT}`);
});