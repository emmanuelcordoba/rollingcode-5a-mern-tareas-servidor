const mongoose = require('mongoose');
const Tarea = require('./Tarea');

// Definir el Schema
const EtiquetaSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
})

EtiquetaSchema.pre('remove', async function() {
    await Tarea.deleteMany({ etiqueta: this._id }).exec();
});

// Define el modelo Etiqueta con el Schema
module.exports = mongoose.model('Etiqueta',EtiquetaSchema);