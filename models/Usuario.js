const mongoose = require('mongoose');

// Definir el Schema
const UsuarioSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
})

// Define el modelo Usuario con el Schema
module.exports = mongoose.model('Usuario',UsuarioSchema);