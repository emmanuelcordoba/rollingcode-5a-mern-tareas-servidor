const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

// Definir el Schema
const TareaSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    descripcion: {
        type: String,
        required: true,
        trim: true
    },
    imagen: {
        type: String,
        trim: true,
        default: null
    },
    etiqueta: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Etiqueta',
        required: true,
    },
    finalizada: {
        type: Boolean,
        default: false
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});

TareaSchema.plugin(mongoosePaginate);

mongoosePaginate.paginate.options = {
    limit: 3,
    customLabels: {
        docs: 'tareas',
        totalDocs: 'totalTareas'
    }
};

// Define el modelo Tarea con el Schema
module.exports = mongoose.model('Tarea',TareaSchema);