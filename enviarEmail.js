const nodemailer = require('nodemailer');

// Crear transporter
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD
    }
});

const enviarEmail = (email, subject, msg) => {

    // Opciones del email
    let mailOptions = {
        to: email,
        subject: subject,
        text: msg,
        html: `
            <p>Hola,</p>
            <p>${msg}</p>
            <p>Saludos</p>
        `
    }

    // Envio del email
    return transporter.sendMail(mailOptions);
}

module.exports = enviarEmail;