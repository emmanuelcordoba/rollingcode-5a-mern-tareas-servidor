const jwt = require('jsonwebtoken');

module.exports = (req,res,next) => {
    // Leer el token del header
    const token = req.header('x-auth-token');
    
    // Controlar si no hay token
    if(!token){
        return res.status(401).json({ msg: 'No hay token, acceso denegado.'});
    }

    // Validar el token
    try {
        const cifrado = jwt.verify(token, process.env.SECRET);
        req.usuario = cifrado.usuario;
        next();
    } catch (error) {
        return res.status(401).json({ msg: 'Token no válido, acceso denegado.'});
    }

}